package be.kdg.java2.relationsdemowithjpa.repository.jpa;

import be.kdg.java2.relationsdemowithjpa.domain.School;
import be.kdg.java2.relationsdemowithjpa.domain.Student;
import be.kdg.java2.relationsdemowithjpa.repository.StudentRepository;
import jakarta.persistence.*;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Profile("jpa")
public class JPAStudentRepository implements StudentRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Student> findAll() {
        List<Student> students = em.createQuery("select student from Student student", Student.class)
                .getResultList();
        return students;
    }

    @Override
    public List<Student> findBySchool(int schoolid) {
        TypedQuery<Student> query = em.createQuery("select student from Student student where student.school.id = :school_id", Student.class);
        query.setParameter("school_id", schoolid);
        List<Student> students = query.getResultList();
        return students;
    }

    @Override
    public List<Student> findByCourse(int courseId) {
        TypedQuery<Student> query = em.createQuery("select student from Student student join student.courses course where course.id = :course_id", Student.class);
        query.setParameter("course_id", courseId);
        List<Student> students = query.getResultList();
        return students;
    }

    @Override
    public Student findById(int id) {
        Student student = em.find(Student.class, id);
        return student;
    }

    @Override
    @Transactional
    public Student createStudent(Student student) {
        em.persist(student);;
        return student;
    }

    @Override
    @Transactional
    public void updateStudent(Student student) {
        em.merge(student);
    }

    @Override
    @Transactional
    public void deleteStudent(int id) {
        em.remove(em.find(Student.class, id));
    }
}
