package be.kdg.java2.relationsdemowithjpa.repository.jdbc;

import be.kdg.java2.relationsdemowithjpa.domain.School;
import be.kdg.java2.relationsdemowithjpa.repository.SchoolRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Profile("jdbc")
public class JDBCSchoolRepository implements SchoolRepository {
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert inserter;

    public JDBCSchoolRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.inserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("SCHOOLS")
                .usingGeneratedKeyColumns("ID");
    }

    //Helper method: maps the columns of the DB to the attributes of the Student
    public static School mapSchoolRow(ResultSet rs, int rowid) throws SQLException {
        return new School(rs.getInt("ID"),
                rs.getString("NAME"));
    }

    @Override
    public List<School> findAll() {
        return jdbcTemplate.query("SELECT * FROM SCHOOLS", JDBCSchoolRepository::mapSchoolRow);
    }

    @Override
    public School findById(int id) {
        return jdbcTemplate.queryForObject("SELECT * FROM SCHOOLS WHERE ID = ?", JDBCSchoolRepository::mapSchoolRow, id);
    }

    @Override
    public School createSchool(School school) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", school.getName());
        school.setId(inserter.executeAndReturnKey(parameters).intValue());
        return school;
    }

    @Override
    public void updateSchool(School school) {
        jdbcTemplate.update("UPDATE SCHOOLS SET NAME=? WHERE ID=?",
                school.getName(), school.getId());
    }

    @Override
    @Transactional
    public void deleteSchool(int id) {
        //should we delete all the students of that school? (school_id cannot be null!)
        //we need to delete the addresses also, and delete student from the courses!
        jdbcTemplate.update("DELETE FROM ADDRESS WHERE STUDENT_ID IN (SELECT STUDENT_ID FROM STUDENTS WHERE SCHOOL_ID=?)", id);
        jdbcTemplate.update("DELETE FROM STUDENTS_COURSES WHERE STUDENT_ID IN (SELECT STUDENT_ID FROM STUDENTS WHERE SCHOOL_ID=?)", id);
        jdbcTemplate.update("DELETE FROM STUDENTS WHERE SCHOOL_ID=?", id);
        jdbcTemplate.update("DELETE FROM SCHOOLS WHERE ID=?", id);
        //to demonstrate the @Transactional, replace above line by this:
        //jdbcTemplate.update("DELETE FROM SCHOOLS WHERE ID=? AND DESCRIPTION = 'ok'", id);
    }
}
