package be.kdg.java2.relationsdemowithjpa.repository.classicjdbc;

import be.kdg.java2.relationsdemowithjpa.domain.Student;
import be.kdg.java2.relationsdemowithjpa.repository.StudentRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@Profile("classic")
public class ClassicJDBCStudentRepository implements StudentRepository {
    @Override
    public List<Student> findAll() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:postgresql:studentsdb", "postgres", "su");
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM STUDENTS");
            List<Student> students = new ArrayList<>();
            while (rs.next()) {
                Student student = new Student(rs.getInt("ID"), rs.getString("NAME"),
                        rs.getDouble("LENGTH"), rs.getDate("BIRTHDAY").toLocalDate());
                students.add(student);
            }
            return students;
        } catch (SQLException sqlException) {
            throw new RuntimeException("Problem!"+ sqlException);
        } finally {
//            if (connection!=null) {
//                try {
//                    connection.close();
//                } catch (SQLException e) {
//                    throw new RuntimeException(e);
//                }
//            }
        }
    }

    @Override
    public List<Student> findBySchool(int schoolid) {
        return null;
    }

    @Override
    public List<Student> findByCourse(int courseId) {
        return null;
    }

    @Override
    public Student findById(int id) {
        return null;
    }

    @Override
    public Student createStudent(Student student) {
        return null;
    }

    @Override
    public void updateStudent(Student student) {

    }

    @Override
    public void deleteStudent(int id) {

    }
}
