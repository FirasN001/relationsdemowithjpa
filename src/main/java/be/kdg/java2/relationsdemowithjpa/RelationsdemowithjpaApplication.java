package be.kdg.java2.relationsdemowithjpa;

import be.kdg.java2.relationsdemowithjpa.presentation.StudentMenu;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class RelationsdemowithjpaApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(RelationsdemowithjpaApplication.class, args);
        context.getBean(StudentMenu.class).show();
    }

}
