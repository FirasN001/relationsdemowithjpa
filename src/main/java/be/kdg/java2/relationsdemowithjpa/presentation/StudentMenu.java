package be.kdg.java2.relationsdemowithjpa.presentation;

import be.kdg.java2.relationsdemowithjpa.domain.Address;
import be.kdg.java2.relationsdemowithjpa.domain.Course;
import be.kdg.java2.relationsdemowithjpa.domain.School;
import be.kdg.java2.relationsdemowithjpa.domain.Student;
import be.kdg.java2.relationsdemowithjpa.repository.CourseRepository;
import be.kdg.java2.relationsdemowithjpa.repository.SchoolRepository;
import be.kdg.java2.relationsdemowithjpa.repository.StudentRepository;
import be.kdg.java2.relationsdemowithjpa.service.CourseService;
import be.kdg.java2.relationsdemowithjpa.service.SchoolService;
import be.kdg.java2.relationsdemowithjpa.service.StudentService;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

@Component
public class StudentMenu {
    private Scanner scanner = new Scanner(System.in);
    private StudentService studentService;
    private CourseService courseService;
    private SchoolService schoolService;

    public StudentMenu(StudentService studentService, CourseService courseService, SchoolService schoolService) {
        this.studentService = studentService;
        this.courseService = courseService;
        this.schoolService = schoolService;
    }

    public void show() {
        while (true) {
            System.out.println("Welcome to the Student Management System");
            System.out.println("========================================");
            System.out.println("1) list all students");
            System.out.println("2) add a student");
            System.out.println("3) update a student");
            System.out.println("4) delete a student");
            System.out.println("5) change address of student");
            System.out.println("6) change school of student");
            System.out.println("7) list all students of school");
            System.out.println("8) delete a school");
            System.out.println("9) add student to course");
            System.out.println("10) list students of course");
            System.out.println("11) list courses of student");
            System.out.println("12) delete course");
            System.out.print("Your choice:");
            int choice = Integer.parseInt(scanner.nextLine());
            switch (choice) {
                case 1 -> listAllStudents();
                case 2 -> addStudent();
                case 3 -> updateStudent();
                case 4 -> deleteStudent();
                case 5 -> changeAddressOfStudent();
                case 6 -> changeSchoolOfStudent();
                case 7 -> showAllStudentsOfSchool();
                case 8 -> deleteSchool();
                case 9 -> addStudentToCourse();
                case 10 -> listStudentsOfCourse();
                case 11 -> listCoursesOfStudent();
                case 12 -> deleteCourse();
            }
        }
    }

    private void deleteCourse() {
        courseService.getCourses().forEach(System.out::println);
        System.out.print("Course id:");
        int courseId = Integer.parseInt(scanner.nextLine());
        courseService.deleteCourse(courseId);
    }

    private void listCoursesOfStudent() {
        listAllStudents();
        System.out.print("Student id:");
        int studentid = Integer.parseInt(scanner.nextLine());
        Student student = studentService.getStudent(studentid);
        //Do we expect the student.getCourses to return all courses of that student?
        //In our implementation, we do: when a student is loaded from db, al his courses are also loaded
        //so this wil work:
        student.getCourses().forEach(System.out::println);
    }

    private void listStudentsOfCourse() {
        courseService.getCourses().forEach(System.out::println);
        System.out.print("Course id:");
        int courseId = Integer.parseInt(scanner.nextLine());
        //we could try:
        //Course course = courseRepository.findById(courseId);
        //course.getStudents().forEach(System.out.println);
        //But do we expect the course.getStudents to return all students of that course?
        //In our implementation we don't: when a course is loaded from db, it's students are not automatically loaded
        //only when we really need them we load them, which is now
        studentService.getStudentsForCourse(courseId).forEach(System.out::println);
    }

    private void addStudentToCourse() {
        listAllStudents();
        System.out.print("Student id:");
        int studentid = Integer.parseInt(scanner.nextLine());
        Student student = studentService.getStudent(studentid);
        courseService.getCourses().forEach(System.out::println);
        System.out.print("Course id:");
        int courseId = Integer.parseInt(scanner.nextLine());
        Course course = courseService.getCourse(courseId);
        //student.addCourse(course);//can't do that, because the courses are lazy loaded
        studentService.addCourseToStudent(student.getId(), course.getId());
        studentService.changeStudent(student);
    }

    private void deleteSchool() {
        System.out.println("Id of school:");
        schoolService.getSchools().forEach(System.out::println);
        int schoolid = Integer.parseInt(scanner.nextLine());
        try {
            schoolService.deleteSchool(schoolid);
        } catch (RuntimeException rte) {
            System.out.println("Unable to delete..." + rte.getMessage());
        }
    }

    private void showAllStudentsOfSchool() {
        System.out.println("Id of school:");
        schoolService.getSchools().forEach(System.out::println);
        int schoolid = Integer.parseInt(scanner.nextLine());
        studentService.getStudentsForSchool(schoolid).forEach(System.out::println);
    }

    private void changeSchoolOfStudent() {
        listAllStudents();
        System.out.print("Student id:");
        int studentid = Integer.parseInt(scanner.nextLine());
        Student student = studentService.getStudent(studentid);
        if (student!=null) {
            changeSchoolOfStudent(student);
            studentService.changeStudent(student);
        }
    }

    private void changeAddressOfStudent() {
        listAllStudents();
        System.out.print("Student id:");
        int studentid = Integer.parseInt(scanner.nextLine());
        Student student = studentService.getStudent(studentid);
        if (student!=null) {
            System.out.print("Street:");
            String street = scanner.nextLine();
            System.out.print("Postal code:");
            int postalcode = Integer.parseInt(scanner.nextLine());
            System.out.print("City:");
            String city = scanner.nextLine();
            Address address = new Address(street, postalcode, city);
            student.setAddress(address);
            studentService.changeStudent(student);
        }
    }

    private void listAllStudents() {
        try {
            studentService.getStudents().forEach(System.out::println);
        } catch (RuntimeException dbe) {
            System.out.println("Unable to find all students:");
            System.out.println(dbe.getMessage());
        }
    }

    private void changeSchoolOfStudent(Student student){
        schoolService.getSchools().forEach(System.out::println);
        System.out.println("(E)xisting or (N)ew school?");
        String choice = scanner.nextLine();
        School school = null;
        if (choice.equals("N")) {
            System.out.print("Name of school:");
            String schoolName = scanner.nextLine();
            school = schoolService.addSchool(new School(schoolName));
        } else {
            System.out.println("Id of school:");
            int schoolid = Integer.parseInt(scanner.nextLine());
            school = schoolService.getSchool(schoolid);
        }
        //This wil not work, we need a managed entity to do that
        //student.setSchool(school);
        studentService.setSchoolOfStudent(student.getId(), school.getId());
    }

    private void addStudent() {
        System.out.print("Name:");
        String name = scanner.nextLine();
        System.out.print("Length:");
        double length = Double.parseDouble(scanner.nextLine());
        System.out.print("Birthday (dd-mm-yyyy):");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate birthday = LocalDate.parse(scanner.nextLine(), formatter);
        Student student = new Student(name, length, birthday);
        try {
            Student createdStudent
                    = studentService.addStudent(student);
            System.out.println("Student added to database:" + createdStudent);
        } catch (RuntimeException dbe) {
            System.out.println("Problem:" + dbe.getMessage());
        }
        changeSchoolOfStudent(student);
    }

    private void updateStudent() {
        listAllStudents();
        System.out.print("Id:");
        int id = Integer.parseInt(scanner.nextLine());
        System.out.print("Name:");
        String name = scanner.nextLine();
        System.out.print("Length:");
        double length = Double.parseDouble(scanner.nextLine());
        System.out.print("Birthday (dd-mm-yyyy):");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate birthday = LocalDate.parse(scanner.nextLine(), formatter);
        Student student = new Student(id, name, length, birthday);
        try {
            studentService.changeStudent(student);
        } catch (RuntimeException dbe) {
            System.out.println("Problem:" + dbe.getMessage());
        }
        changeSchoolOfStudent(student);
    }

    private void deleteStudent() {
        listAllStudents();
        System.out.print("Id:");
        int id = Integer.parseInt(scanner.nextLine());
        try {
            studentService.deleteStudent(id);
        } catch (RuntimeException dbe) {
            System.out.println("Problem:" + dbe.getMessage());
        }
    }
}
