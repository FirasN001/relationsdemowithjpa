package be.kdg.java2.relationsdemowithjpa.service;

import be.kdg.java2.relationsdemowithjpa.domain.Course;
import be.kdg.java2.relationsdemowithjpa.domain.Student;
import be.kdg.java2.relationsdemowithjpa.repository.CourseRepository;
import be.kdg.java2.relationsdemowithjpa.repository.StudentRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CourseServiceImpl implements CourseService {
    private CourseRepository courseRepository;
    private StudentRepository studentRepository;

    public CourseServiceImpl(CourseRepository courseRepository, StudentRepository studentRepository) {
        this.courseRepository = courseRepository;
        this.studentRepository = studentRepository;
    }

    @Override
    @Transactional
    public void deleteCourse(int id) {
        //student is in charge, so deleting the course will not delete the jointabel records
        //so we first have to remove the course from each student that follows it
        //then we can delete the course...
        Course course = courseRepository.findById(id);
        course.getStudents().forEach(student->{
            student.getCourses().remove(course);
            studentRepository.updateStudent(student);
        });
        courseRepository.deleteCourse(id);
    }

    @Override
    public List<Course> getCourses() {
        return courseRepository.findAll();
    }

    @Override
    public Course getCourse(int courseId) {
        return courseRepository.findById(courseId);
    }
}
