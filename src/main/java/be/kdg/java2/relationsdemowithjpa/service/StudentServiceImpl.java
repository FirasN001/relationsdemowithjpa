package be.kdg.java2.relationsdemowithjpa.service;

import be.kdg.java2.relationsdemowithjpa.domain.Course;
import be.kdg.java2.relationsdemowithjpa.domain.School;
import be.kdg.java2.relationsdemowithjpa.domain.Student;
import be.kdg.java2.relationsdemowithjpa.repository.CourseRepository;
import be.kdg.java2.relationsdemowithjpa.repository.SchoolRepository;
import be.kdg.java2.relationsdemowithjpa.repository.StudentRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService{
    private StudentRepository studentRepository;
    private SchoolRepository schoolRepository;
    private CourseRepository courseRepository;

    public StudentServiceImpl(StudentRepository studentRepository, SchoolRepository schoolRepository, CourseRepository courseRepository) {
        this.studentRepository = studentRepository;
        this.schoolRepository = schoolRepository;
        this.courseRepository = courseRepository;
    }

    public Student addStudent(Student student) {
        return studentRepository.createStudent(student);
    }

    public void changeStudent(Student student){
        studentRepository.updateStudent(student);
    }

    @Override
    public void deleteStudent(int id) {
        studentRepository.deleteStudent(id);
    }

    @Override
    public List<Student> getStudents() {
        return studentRepository.findAll();
    }

    @Override
    public Student getStudent(int id) {
        return studentRepository.findById(id);
    }

    @Override
    public List<Student> getStudentsForCourse(int courseId) {
        return studentRepository.findByCourse(courseId);
    }

    @Override
    public List<Student> getStudentsForSchool(int schoolid) {
        return studentRepository.findBySchool(schoolid);
    }

    @Override
    @Transactional
    public void addCourseToStudent(int studentId, int courseId) {
        Student student = studentRepository.findById(studentId);
        Course course = courseRepository.findById(courseId);
        student.addCourse(course);
        studentRepository.updateStudent(student);
    }

    @Override
    @Transactional
    public void setSchoolOfStudent(int studentId, int schoolId) {
        Student student = studentRepository.findById(studentId);
        if (student!=null) {
            School school = schoolRepository.findById(schoolId);
            if (school!=null) {
                student.setSchool(school);
                studentRepository.updateStudent(student);
            }
        }
    }
}
