package be.kdg.java2.relationsdemowithjpa.service;

import be.kdg.java2.relationsdemowithjpa.domain.Course;

import java.util.List;

public interface CourseService {
    void deleteCourse(int id);
    List<Course> getCourses();

    Course getCourse(int courseId);
}
