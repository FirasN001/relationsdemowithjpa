package be.kdg.java2.relationsdemowithjpa.domain;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SCHOOLS")
public class School {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

    //One-to-Many relationship
    //But do we need this attribute?
    //If we wanna be able to save a school and as a consequence
    //update all the students
    @OneToMany(mappedBy = "school", cascade = CascadeType.REMOVE)
    private List<Student> students = new ArrayList<>();

    protected School() {
    }

    public School(String name) {
        this.name = name;
    }

    public School(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void addStudent(Student s){
        if (students==null) {
            students = new ArrayList<>();
        }
        students.add(s);
    }

    @Override
    public String toString() {
        return "School{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
